﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

public class WeaponZoom : MonoBehaviour
{
    [SerializeField] private RigidbodyFirstPersonController fpsController;
    [SerializeField] private float zoomInFOV = 20.0f;
    [SerializeField] private float zoomOutFOV = 60.0f;
    [SerializeField] private float ZoomInXSensitivity;
    [SerializeField] private float ZoomInYSensitivity;

    private float xSensitivity, ySensitivity;
    private Camera mainCamera;
    private bool zoomInToggle = false;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera= Camera.main;
        xSensitivity = fpsController.mouseLook.XSensitivity;
        ySensitivity = fpsController.mouseLook.YSensitivity;
    }

    // Update is called once per frame
    void Update()
    {
        ToggleZoomInAndOut();
    }
    private void ToggleZoomInAndOut()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (!zoomInToggle)
            {
                ZoomIn();
            } else
            {
                ZoomOut();
            }
        }
    }

    private void ZoomOut()
    {
        zoomInToggle = false;
        mainCamera.fieldOfView = zoomOutFOV;
        fpsController.mouseLook.XSensitivity = xSensitivity;
        fpsController.mouseLook.YSensitivity = ySensitivity;
    }

    private void ZoomIn()
    {
        zoomInToggle = true;
        mainCamera.fieldOfView = zoomInFOV;
        fpsController.mouseLook.XSensitivity = ZoomInXSensitivity;
        fpsController.mouseLook.YSensitivity = ZoomInYSensitivity;
    }

    private void MyZoom()
    {
        if (Input.GetMouseButtonDown(1))
        {
            print("ZoomIn");
            zoomInToggle = true;
            mainCamera.fieldOfView = zoomInFOV;
        }

        if (Input.GetMouseButtonUp(1))
        {
            print("ZoomOut");
            zoomInToggle = false;
            mainCamera.fieldOfView = zoomOutFOV;
        }
    }

    private void OnDisable()
    {
        ZoomOut();
    }
}
