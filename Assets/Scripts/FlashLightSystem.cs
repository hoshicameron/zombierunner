﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class FlashLightSystem : MonoBehaviour
{
   [SerializeField] private float lightDecay = .1f;//Amount of decrement light intensity over time
   [SerializeField] private float angleDecay = 1f;
   [SerializeField] private float minLightAngle=40f;
   [SerializeField] private float chargingDelay=10f;
   [Header("light Off recharging delay")]
   [SerializeField] private float delayLightOffCharging = 5f;

   private Light myLight;
   private float startIntensity;
   private float startAngle;
   private bool isCharging = false;
   private bool isUsingFlashLight = false;
   private float lightOffTime;
   private void Start()
   {
      myLight = GetComponent<Light>();

      startAngle = myLight.spotAngle;
      startIntensity = myLight.intensity;
      myLight.enabled = false;
      lightOffTime = Mathf.Infinity;
   }

   private void Update()
   {
      if (Input.GetKeyDown(KeyCode.F) &&!isCharging)
      {
         OnOffFlashLight();
      }

      if (isUsingFlashLight)
      {
         DecreaseLightAngle();
         DecreaseLightIntensity();
      }

      if (lightOffTime <= Time.time && !isUsingFlashLight)
      {
         lightOffTime = Mathf.Infinity;
         StartCoroutine(RechargingBattery(0));
      }


   }

   private void OnOffFlashLight()
   {
      myLight.enabled = !myLight.enabled;
      isUsingFlashLight = !isUsingFlashLight;

      if (!isUsingFlashLight)
      {
         lightOffTime = Time.time + delayLightOffCharging;
      }
   }

   private void DecreaseLightIntensity()
   {
      if (myLight.intensity <= Mathf.Epsilon)
      {
         if (!isCharging)
         {
            //Start Charging Sequence"
            StartCoroutine(RechargingBattery(chargingDelay));
         }
         return;
      }
      myLight.intensity -= lightDecay*Time.deltaTime;
   }

   private void DecreaseLightAngle()
   {
      if(myLight.spotAngle<=minLightAngle)   return;

      myLight.spotAngle -= angleDecay*Time.deltaTime;
   }

   IEnumerator RechargingBattery(float delayToRecharge)
   {
      isCharging = true;
      isUsingFlashLight = false;
      myLight.enabled = false;
      yield return new WaitForSeconds(delayToRecharge);
      myLight.intensity = startIntensity;
      myLight.spotAngle = startAngle;
      isCharging = false;
   }
}
