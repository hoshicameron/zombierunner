﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    private Animator anim;
    private EnemyAI enemyAI;
    private EnemyHealth enemyHealth;
    private int moveParamID;
    private int attackParamID;
    private int dieParamID;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        enemyAI = GetComponent<EnemyAI>();
        moveParamID = Animator.StringToHash("Move");
        attackParamID = Animator.StringToHash("Attack");
        dieParamID = Animator.StringToHash("Die");
        enemyHealth = GetComponent<EnemyHealth>();

    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool(moveParamID,enemyAI.isChasing);
        if (enemyAI.isAttacking)
        {
            anim.SetTrigger(attackParamID);
        }
    }

    public void ActiveDeadAnimation()
    {
        if(enemyHealth.IsDead)     return;

        enemyHealth.IsDead = true;
        anim.SetTrigger(dieParamID);
    }
}
