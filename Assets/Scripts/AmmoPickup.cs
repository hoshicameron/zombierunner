﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    [SerializeField] private AmmoType ammoType;
    [SerializeField] private int ammoAmount=10;
    private int playerLayer;

    private void Start()
    {
        playerLayer = LayerMask.NameToLayer("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer!=playerLayer)    return;

        other.gameObject.GetComponent<Ammo>().IncreaseCurrentAmmo(ammoType,ammoAmount);
        Destroy(gameObject);

    }
}
