﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitcher : MonoBehaviour
{
    [SerializeField] private int currentWeaponIndex = 0;
    [SerializeField] private List<Weapon> weapons=new List<Weapon>();

    // Start is called before the first frame update
    void Start()
    {
        SetWeaponActive();
    }

    void Update()
    {
        int previousWeapon = currentWeaponIndex;

        ProcessKeyInput();
        ProcessScroolWheel();

        if(previousWeapon!=currentWeaponIndex)
            SetWeaponActive();
    }

    private void ProcessScroolWheel()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (currentWeaponIndex >= weapons.Count - 1)
                currentWeaponIndex = 0;
            else
            {
                currentWeaponIndex++;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (currentWeaponIndex <= 0)
                currentWeaponIndex = weapons.Count-1;
            else
            {
                currentWeaponIndex--;
            }
        }
    }

    private void ProcessKeyInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentWeaponIndex = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentWeaponIndex = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentWeaponIndex = 2;
        }
    }

    private void SetWeaponActive()
    {
        for (int i = 0; i < weapons.Count; i++)
        {
            weapons[i].gameObject.SetActive(i == currentWeaponIndex);
        }
    }

}
