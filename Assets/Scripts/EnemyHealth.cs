﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class EnemyHealth : MonoBehaviour
{
   [SerializeField] private float hitPoints = 100f;
   private bool isDead = false;

   public bool IsDead
   {
      get { return isDead; }
      set { isDead = value; }
   }

   public void TakeDamage(float damage)
   {
      hitPoints -= damage;
      BroadcastMessage("OnDamageTaken");
      if (hitPoints <= 0)
      {
         SendMessage("ActiveDeadAnimation");
         IsDead = true;
         //Destroy(gameObject,3.0f);
      }
   }
}
