﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets.CrossPlatformInput;

[DisallowMultipleComponent]
public class Weapon : MonoBehaviour
{
    #region Declare Variables
    [SerializeField] private Camera fpCamera;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private GameObject hitImpactVFX;
    [SerializeField] private Ammo ammoSlot;
    [SerializeField] private AmmoType ammoType;
    [SerializeField] private TextMeshProUGUI weaponUI;

    [SerializeField] private float range = 100f;
    [SerializeField] private float weaponDamage = 10f;
    [FormerlySerializedAs("weaponDelay")] [SerializeField] private float delayBetweenShoots = 0.5f;
    [SerializeField] private float impactLifeTime = 1.0f;


    private bool canShoot = true;
    #endregion

    private void OnEnable()
    {
        canShoot = true;
        weaponUI.text = ammoSlot.GetAmmoAmount(ammoType).ToString();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canShoot)
        {
            StartCoroutine(nameof(Shoot));
        }
    }


    IEnumerator Shoot()
    {
        canShoot = false;
        if (ammoSlot.GetAmmoAmount(ammoType) > 0)
        {
            ammoSlot.ReduceCurrentAmmo(ammoType);
            weaponUI.text = ammoSlot.GetAmmoAmount(ammoType).ToString();
            PlayMuzzleFlash();
            RaycastProcess();
        }
        yield return new WaitForSeconds(delayBetweenShoots);
        canShoot = true;
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlash.Play();
    }

    private void RaycastProcess()
    {
        Transform fpCameraTransform = fpCamera.transform;
        Ray ray = new Ray(fpCameraTransform.position, fpCameraTransform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range))
        {
            CreateHitImpact(hit);
            //TODO add some effect to visual players
            EnemyHealth target = hit.collider.GetComponent<EnemyHealth>();

            if(target==null) return;

            target.TakeDamage(weaponDamage);
        }
    }

    private void CreateHitImpact(RaycastHit hit)
    {
        Vector3 hitPosition = hit.point;
        GameObject impact= Instantiate(hitImpactVFX, hitPosition, Quaternion.LookRotation(hit.normal))as GameObject;
        Destroy(impact,impactLifeTime);
    }
}
