﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayDamage : MonoBehaviour
{
    [SerializeField] private Canvas displayDamageCanvas;
    [SerializeField] private float damageUIShowTime = 2.0f;


    // Start is called before the first frame update
    void Start()
    {
        displayDamageCanvas.enabled = false;
    }

    public void HandleDamage()
    {
        StartCoroutine(ShowDamageUI());
    }

    IEnumerator ShowDamageUI()
    {
        displayDamageCanvas.enabled = true;
        yield return new  WaitForSeconds(damageUIShowTime);
        displayDamageCanvas.enabled = false;
    }
}
