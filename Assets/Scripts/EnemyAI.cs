﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[DisallowMultipleComponent]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    #region DEclare Properties

    private Transform target;
    [SerializeField] private float chaseRange = 15f;
    [SerializeField] private float turnSpeed=10f;

    private NavMeshAgent agent;
    private float distanceToTarget = Mathf.Infinity;
    private bool isProvoked=false;

    private bool _isAttacking=false;
    private bool _isChasing=false;


    public bool isAttacking
    {
        get { return _isAttacking; }
        set { _isAttacking = value; }
    }

    public bool isChasing
    {
        get { return _isChasing; }
        set { _isChasing = value; }
    }

    private EnemyHealth enemyHealth;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = FindObjectOfType<PlayerHealth>().transform;
        enemyHealth = GetComponent<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth.IsDead)
        {
            enabled = false;
            agent.enabled = true;

        };

        distanceToTarget = Vector3.Distance(target.position, transform.position);
        if (isProvoked)
        {
            EngageTarget();
        }
        else if (distanceToTarget <= chaseRange)
        {
            isProvoked = true;
        }
    }

    public void OnDamageTaken()
    {
        isProvoked = true;
    }

    private void EngageTarget()
    {

        if (distanceToTarget>=agent.stoppingDistance)
        {
            ChaseTarget();
        } else
        {
            isChasing = false;
        }

        if (distanceToTarget<=agent.stoppingDistance)
        {
            AttackTarget();
        } else
        {
            isAttacking = false;
        }

    }

    private void AttackTarget()
    {
        FaceTarget();
        isAttacking = true;
    }

    private void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation=Quaternion.LookRotation(new Vector3(direction.x,0,direction.z));
        transform.rotation=Quaternion.Slerp(transform.rotation,lookRotation,Time.deltaTime*turnSpeed);
    }
    private void ChaseTarget()
    {
        agent.SetDestination(target.position);
        isChasing = true;
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = new Color(1f, 0.24f, 0.27f, 0.75f);
        Gizmos.DrawWireSphere(transform.position, chaseRange);
    }
}
