﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

public class Ammo : MonoBehaviour
{
    [System.Serializable]
    private class AmmoSlot
    {
        public AmmoType ammoType;
        public int ammoAmount;
    }

    [SerializeField] private AmmoSlot[] ammoSlots;

    public int GetAmmoAmount(AmmoType ammoType)
    {
        return GetAmmoSlot(ammoType).ammoAmount;
    }

    public void ReduceCurrentAmmo(AmmoType ammoType )
    {
        GetAmmoSlot(ammoType).ammoAmount--;
    }

    public void IncreaseCurrentAmmo(AmmoType ammoType,int ammoToAdd)
    {
        GetAmmoSlot(ammoType).ammoAmount += ammoToAdd;
    }


    private AmmoSlot GetAmmoSlot(AmmoType ammoType)
    {
        for (int i = 0; i < ammoSlots.Length; i++)
        {
            if (ammoSlots[i].ammoType == ammoType)
            {
                return ammoSlots[i];
            }
        }

        return null;
    }


}
