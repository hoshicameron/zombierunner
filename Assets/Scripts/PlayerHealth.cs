﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float playerHelath = 100f;
    private DeathHandler deathHandler;

    private void Start()
    {
        deathHandler= GetComponent<DeathHandler>();
    }

    public void TakeDamage(float damage)
    {
        playerHelath -= damage;
        GetComponent<DisplayDamage>().HandleDamage();
        if(playerHelath<=0)
        {
            deathHandler.HandleDeath();
        }
    }
}
